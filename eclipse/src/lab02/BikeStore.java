package lab02;
// Jackson Kwan 1938023
public class BikeStore {

	public static void main(String[] args) {
		Bicycle[] bikeArr = new Bicycle[4];
		bikeArr[0] = new Bicycle("Volv", 3, 43);
		bikeArr[1] = new Bicycle("Bolo", 2, 30);
		bikeArr[2] = new Bicycle("Montan", 4, 52);
		bikeArr[3] = new Bicycle("Conten", 1, 25);
		
		for(int i=0; i<bikeArr.length; i++) {
			System.out.println(bikeArr[i].toString());
		}
	}

}
