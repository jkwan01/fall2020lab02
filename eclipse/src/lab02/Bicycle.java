package lab02;
// Jackson Kwan 1938023
public class Bicycle {
	private String manufacturer;
	private int numberGears;
	private double maxSpeed;
	
	//getters
	public String getManufacturer() {
		return manufacturer;
	}
	public int getNumberGears() {
		return numberGears;
	}
	public double getMaxSpeed() {
		return maxSpeed;
	}
	
	//constructors
	public Bicycle() {
		manufacturer = "";
		numberGears = 0;
		maxSpeed = 0;
	}
	
	public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
		this.manufacturer = manufacturer;
		this.numberGears = numberGears;
		this.maxSpeed = maxSpeed;
	}
	
	//toString overriding
	public String toString() {
		return ("Manufacturer: " + manufacturer + " | Number of Gears: " + numberGears + " | Max Speed: " + maxSpeed);
	}
}
